// ============ Preloader ============

var $window = $(window);
$window.on('load', function () {
  $('#preloader').fadeOut('slow', function () {
      $(this).remove();
  });
});

// ============ navbar shrink ============
$(document).on("scroll", function(){
  if
    ($(document).scrollTop() > 300){
    $("header").addClass("animated");
    $("header").addClass("fadeInDown");
    $("header").addClass("shrink");   
    $('.scroll-to-top').fadeIn(); 
  }
  else
  {
    $("header").removeClass("animated");
    $("header").removeClass("fadeInDown");
    $("header").removeClass("shrink");
    $('.scroll-to-top').fadeOut();     
  }
});

// ============ Smooth Scroll and Active Navbar============
var scroll = new SmoothScroll('a[href*="#"]');

$('[data-spy="scroll"]').each(function () {
  var $spy = $(this).scrollspy('refresh')
})

// ============ Hover effect benefit section ============
$(".benefit-item").hover(function() {
  $("h3.active").removeClass("active");
  $(this).find("h3").addClass("active");
});

// ============ Hover effect features section ============
$(".feature-item").hover(function() {
  $("h3.active").removeClass("active");
  $(this).find("h3").addClass("active");
});

// ============ CounterUp ============
var options = {
    useEasing: true, 
    separator: ',', 
    decimal: '.', 
  };
  var demo = new CountUp('counter', 0, 12891476, 0, 2.5, options);
  if (!demo.error) {
    demo.start();
  } else {
    console.error(demo.error);
  }

// ============ CounterUp ============
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

// ============ Owl-carousel ============
var $owl = $('#screenshot');

$owl.children().each( function( index ) {
  $(this).attr( 'data-position', index ); // NB: .attr() instead of .data()
});

$owl.owlCarousel({
  center: true,
  loop: true,
  items: 5,
});

$(document).on('click', '.owl-item>div', function() {
  $owl.trigger('to.owl.carousel', $(this).data( 'position' ) );
});


$('#testimonial').owlCarousel({
  items: 1,
  margin: 0,
  loop: true,
  nav: true,
  navText: ['<i class="ti-arrow-left"></i>', '<i class="ti-arrow-right"></i>'],
  dots: true,
  autoplay: false,
  smartSpeed: 800
});

// ============ Init animate ============
    // Init AOS
      AOS.init();
    // Init WOW
    new WOW().init();






